﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFall : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameObject.FindWithTag("Faller").GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}
