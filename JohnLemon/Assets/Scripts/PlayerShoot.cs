﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public GameObject bulletPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if (Input.GetMouseButton(0)) {
            GameObject bulletObject = Instantiate (bulletPrefab); //Spawns bullet prefab
            bulletObject.transform.position = transform.position + transform.forward + transform.up; //Bullet begins infront and at eye level with Player
            bulletObject.transform.forward = transform.forward;
        } 
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
        }
    }
}

